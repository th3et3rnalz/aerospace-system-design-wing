import numpy as np
import scipy as sp
from scipy import interpolate
import matplotlib.pyplot as plt
from file_ingestion import ingest
from Forces import get_shear_and_moment, get_torque_diagram

# CAUTION: PROGRAM CALCULATES STIFFENER/RIB LOCATIONS ONLY FOR FRONT SPAR
# CONTACT AUTHOR (Wouter van der Sluis) TO CHANGE TO REAR SPAR

# Load lift distribution datafiles
a0 = ingest("Datafiles/MainWinga0.txt")
a10 = ingest("Datafiles/MainWinga10.txt")

# Set load factor
n = 2.866

# Set safety factor:
k_safety = 1.5

if n < 2.866:
    print("WARNING: Load factor less than 2.866, program may work incorrectly.")
    print("Please contact program author.")

# Load shear, moment, torque functions
shear, moment = get_shear_and_moment(a0, a10, show_plots=False, n=n)
torque = get_torque_diagram(a0, a10, n=n, show_plots=False)

# Configuration parameters
tf = 0.0025
tr = 0.0025

# shear_max/shear_avg:
kfactor = 1.5

# Material properties
poisson = 1 / 3
E = 72.4 * 10 ** 9


# Conversion of k_s graph from reader to callable python code:
def k_s(a, b, clamped=False):
    # k_s value was manually measured for each of the following a/b values
    # Note: some values were added at the start and end to correct the graphs
    ab = [1, 1.03, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 3, 3.5, 4, 5]

    # k_s values for clamped graph (not used)
    ks_clamped = [16, 15, 13.8, 13, 12.3, 11.8, 11.5, 11.3, 11, 10.8, 10.5, 10.3, 10.2, 10, 9.9, 9.8, 9.8, 9.7, 9.6, 9.5, 9.4, 9.4]
    ks_interp_clamped = sp.interpolate.interp1d(ab, ks_clamped, kind='quadratic', fill_value="extrapolate")

    # k_s values for hinged graph
    ks_hinged = [10, 9.6, 9.0, 8.2, 7.6, 7.3, 7.0, 6.85, 6.7, 6.6, 6.5, 6.4, 6.3, 6.25, 6.17, 6.1, 6.0, 5.95, 5.8, 5.7, 5.6, 5.4]
    ks_interp_hinged = sp.interpolate.interp1d(ab, ks_hinged, kind='quadratic', fill_value="extrapolate")

    # For a/b values beyond 5 (outside graph scope) it was assumed that
    # the k_s is equal to the k_s corresponding to a/b = 5
    # Note: this was only done for the hinged graph since the clamped graph is not used
    if clamped and a/b < 5:
        return ks_interp_clamped(a / b)
    elif not clamped and a/b <= 5:
        return ks_interp_hinged(a / b)
    elif not clamped and a/b > 5:
        return 5.4


# Calculate critical buckling shear stress
def critshear(height, width, pois, e_mod, t):
    # Width and height are switched when the height becomes longer than the width
    # This is because k_s graph is not valid for a/b < 0
    if width/height < 1:
        temp = width
        width = height
        height = temp

    # Return shear stress using formula from reader
    return np.pi ** 2 * k_s(width, height) * e_mod / (12 * (1 - pois ** 2)) * (t / height) ** 2


# Calculate maximum shear stress in spar
def shear_max_f(y, V, T, tf, tr, k_v, front=True):
    # Calculate local chord length
    c = 3.07 + (1.22 - 3.07) / (17.85 / 2) * y

    # Calculate local wing box element dimensions from element-to-chord-length ratios:
    hf = c * 0.531 / 3.07
    hr = c * 0.327 / 3.07
    l_top = c * 1.636 / 3.07
    l_bot = c * 1.637 / 3.07

    # Calculate local enclosed cross-sectional area:
    A1 = l_top * np.cos(np.radians(3.775)) * hr
    A2 = 0.5 * l_top * np.cos(np.radians(3.775)) * l_top * np.sin(np.radians(3.775))
    A3 = 0.5 * l_bot * np.cos(np.radians(3.36)) * l_bot * np.sin(np.radians(3.36))
    Ai = A1 + A2 + A3

    # Calculate shear stress due to torsion using formula from reader:
    if front:
        shear_torsion = T(y) / (2 * Ai * tf)
    else:
        shear_torsion = - T(y) / (2 * Ai * tr)

    # Calculate shear stress due to shear force using formula from reader:
    shear_avg = V(y) / (hf * tf + hr * tr)

    # Return total shear stress including peak factor:
    return k_safety * (k_v * shear_avg + shear_torsion)


# Produce maximum shear stress graph (for testing):
def shear_max_graph(y1=0, y2=17.85/2, front=True):
    xarray = []
    yarray = []
    for i in np.arange(y1, y2, 0.001):
        xarray.append(i)
        yarray.append(shear_max_f(i, shear, torque, tf, tr, kfactor, front))
    plt.plot(xarray, yarray)
    plt.show()


# Produce interpolated k_s graph (for testing):
def k_s_graph(ab1, ab2):
    xarray = []
    y1array = []
    y2array = []
    for i in np.arange(ab1, ab2, 0.001):
        xarray.append(i)
        y1array.append(k_s(i, 1, clamped=False))
        y2array.append(k_s(i, 1, clamped=False))
    plt.plot(xarray, y1array)
    plt.plot(xarray, y2array)
    plt.show()


# Calculate minimum section width required to prevent shear buckling:

# Procedure: The function starts at a set y-value,
#            with a starting maximum width, set by the 'if' function.
#            It then reduces this width until the stress margin becomes positive,
#            and returns this width.

# Note:     The maximum section width is calculated using
#           the shear stress and spar height at the initially set y-value.
#           The shear stress is higher closer to the root, so this benefit the strength
#           of the wing box. However, a lower spar height decreases the critical buckling
#           stress. But, since the sections are not large, the difference in inner spar
#           and outer spar heights is very small. For simplicity of code the inner spar height
#           is therefore used.

def minsectionwidth(y0, t, front=True):
    # Calculation of inner spar height using local chord length:
    c = 3.07 + (1.22 - 3.07) / (17.85 / 2) * y0

    if front:
        h0 = c * 0.531 / 3.07
    else:
        h0 = c * 0.327 / 3.07

    # Code optimization to reduce calculation time:
    # WARNING: This will probably give errors if the load factor n is set to below 2.866
    if y0 < 2:
        max_width = 0.15
    elif y0 < 5:
        max_width = 0.2
    elif y0 < 7:
        max_width = 0.5
    else:
        max_width = 2

    # Margin between critical buckling stress and local maximum buckling stress:
    margin = critshear(h0, max_width, poisson, E, t) - shear_max_f(y0, shear, torque, tf, tr, kfactor, front)

    # Reduction of section width until margin becomes positive:
    while margin < 0:
        max_width = max_width - 0.001
        margin = critshear(h0, max_width, poisson, E, t) - shear_max_f(y0, shear, torque, tf, tr, kfactor, front)

    # Return maximum possible section width:
    return max_width


# Array holding maximum stiffener/rib y-values:
y_arr = [0]

# Loop to calculate y-values for entire wing:
while y_arr[-1] < 17.85 / 2:
    element_location = minsectionwidth(y_arr[-1], tf, True)
    y_arr.append(round(y_arr[-1] + element_location, 4))

# Correction of array to remove stiffener/rib placed beyond wing tip y-value:
if y_arr[-1] > 17.85 / 2:
    del y_arr[-1]

y_arr.append(round(17.85/2, 4))

# Print minimum number of stiffeners/ribs required: (Sanity check)
print("Minimum number of stiffeners (including root and tip): " + str(len(y_arr)))

# Print array holding optimal stiffener/rib locations:
print(y_arr)
