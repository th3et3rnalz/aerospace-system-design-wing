from file_ingestion import ingest
from vn import make_vn
import numpy as np

a0 = ingest("Datafiles/MainWinga0.txt")
a10 = ingest("Datafiles/MainWinga10.txt")
n_values = [-1, 2.866]
fuel_percentage = [0.3, 0.86, 1.0]
h_cruise = 12496.8


# ======================================================================================================================
# SHEAR AND TORQUE DIAGRAMS     SHEAR AND TORQUE DIAGRAMS     SHEAR AND TORQUE DIAGRAMS     SHEAR AND TORQUE DIAGRAMS
# ======================================================================================================================
# from wingWeightDistribution import graph_fuel_weight_vs_span, graph_wing_weight_vs_span
#
#
# graph_fuel_weight_vs_span()
# graph_wing_weight_vs_span()
#
#
# from Forces import get_shear_and_moment, get_torque_diagram
#
#
# for n in n_values:
#     _, _ = get_shear_and_moment(a0, a10, show_plots=True, n=n, fuels=fuel_percentage)
#     _ = get_torque_diagram(a0, a10, n=n)


# ======================================================================================================================
#  VN+GUST LOAD DIAGRAMS  VN+GUST LOAD DIAGRAMS  VN+GUST LOAD DIAGRAMS   VN+GUST LOAD DIAGRAMS  VN+GUST LOAD DIAGRAMS
# ======================================================================================================================
# from gust_loads import make_gust_load
#
#
# for h, condition in [(0, "lift_off"), (h_cruise/2, "climb"), (h_cruise, "cruise"), (h_cruise/2, "descent")]:
#     # The make_vn function shows and saves the plot, the make_gust only adds to the plot
#     make_gust_load(altitude=h, condition=condition)
#     make_vn(altitude=h, condition=condition)

# ======================================================================================================================
#  DEFLECTION AND ROTATION CALCULATIONS    DEFLECTION AND ROTATION CALCULATIONS    DEFLECTION AND ROTATION CALCULATIONS
# ======================================================================================================================
# from stiffness_calc import bendingDisplacement, TwistDisplacement
# import stress  # Let's make the stress diagrams for the three loading cases
#
#
# # starting x-value of stringer (datum is front spar):
# str_x0_upper = [0, 0.409, 0.817, 1.226, 1.634]
# str_x0_lower = [0, 0.409, 0.817, 1.226, 1.634]
#
# # stringer sweep angle (backward sweep positive):
# str_theta_upper = np.deg2rad([3.822, 2.244, 0.669, -0.912, -2.487])
# str_theta_lower = np.deg2rad([3.822, 2.244, 0.669, -0.912, -2.487])
#
# # stringer section starting y-values:
# str_sec_upper = [0., 4.]
# str_sec_lower = [0., 4.]
#
# croot = 3.07
# ctip = 1.22
# span = 17.85
# t_fspar = 0.001
# t_rspar = 0.001
# t_top = 0.001
# t_bot = 0.001
# str_area = 0.000040
# emodulus = 70 * 10 ** 9
# shearmodulus = 27 * 10 ** 9
#
# bendingDisplacement(croot,
#                     ctip,
#                     span,
#                     t_fspar,
#                     t_rspar,
#                     t_top,
#                     t_bot,
#                     str_area,
#                     str_x0_upper,
#                     str_x0_lower,
#                     np.deg2rad(3.775),
#                     np.deg2rad(3.36),
#                     str_theta_upper,
#                     str_theta_lower,
#                     str_sec_upper,
#                     str_sec_lower,
#                     emodulus)
#
# TwistDisplacement(t_fspar,
#                   t_rspar,
#                   t_top,
#                   t_bot,
#                   croot,
#                   ctip,
#                   span,
#                   shearmodulus)
#
#
