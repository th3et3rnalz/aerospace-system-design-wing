import numpy as np
import scipy as sp
from scipy import interpolate
import os
import matplotlib.pyplot as plt
from file_ingestion import ingest
from Forces import get_shear_and_moment, get_torque_diagram

a0 = ingest("Datafiles/MainWinga0.txt")
a10 = ingest("Datafiles/MainWinga10.txt")
n = 2.866
shear, moment = get_shear_and_moment(a0, a10, show_plots=False, n=n)

# ALL UNITS SI WITH LENGTHS IN METRES

# Static values:
phi1 = 3.775
phi2 = 3.36
croot = 3.07
ctip = 1.22
span = 17.85

# Configuration-specific:
t_fspar = 0.001
t_rspar = 0.001
t_top = 0.001
t_bot = 0.001
str_area = 0.000040

emodulus = 70 * 10 ** 9
shearmodulus = 27 * 10 ** 9

# starting x-value of stringer (datum is front spar):
str_x0_upper = [0, 0.409, 0.817, 1.226, 1.634]
str_x0_lower = [0, 0.409, 0.817, 1.226, 1.634]

# stringer sweep angle (backward sweep positive):
str_theta_upper = np.deg2rad([3.822, 2.244, 0.669, -0.912, -2.487])
str_theta_lower = np.deg2rad([3.822, 2.244, 0.669, -0.912, -2.487])

# stringer section starting y-values:
str_sec_upper = [0., 4.]
str_sec_lower = [0., 4.]

# stringer numbers present in each section:
str_upper = [135, 15]
str_lower = [135, 15]


# calculate wing box moment of inertia at a specific y:
def wingboxmoi(y, cr, ct, b, t_fspar, t_rspar, t_top, t_bot, A_str, str_x0_upper, str_x0_lower, phi1, phi2, str_theta_upper, str_theta_lower, str_sec_upper, str_sec_lower):
    # calculate chord as function of y:
    c = cr + (ct - cr) / (b / 2) * y

    # calculate centroids of individual sections:
    C_top = 241.6 / 3069 * c
    C_bot = -187.6 / 3069 * c
    C_fspar = 24 / 3069 * c
    C_rspar = 29.5 / 3069 * c

    # calculate distance of stringer to chord line:
    dchord_lower = []
    dchord_upper = []

    for i in range(len(str_x0_lower)): # lower stringers
        dchord1 = - 0.242 + str_x0_lower[i] * np.tan(phi1) + y * np.sin(str_theta_lower[i]) * np.sin(phi1) / np.cos(str_theta_lower[i]) + y * (0.21 * cr - 0.21 * ct) / b
        dchord_lower.append(dchord1)

    for j in range(len(str_x0_upper)): # upper stringers
        dchord2 = 0.290 - str_x0_upper[j] * np.tan(phi2) + y * np.sin(str_theta_upper[j]) * np.sin(phi2) / np.cos(str_theta_upper[j]) - y * (0.21 * cr - 0.21 * ct) / b
        dchord_upper.append(dchord2)

    # calculate section lengths:
    l_fspar = c * 531 / 3069
    l_rspar = c * 327 / 3069
    l_top = c * 1636 / 3069
    l_bot = c * 1637 / 3069

    # calculate section areas:
    A_top = l_top * t_top
    A_bot = l_bot * t_bot
    A_fspar = l_fspar * t_fspar
    A_rspar = l_rspar * t_rspar

    # calculate cross-section centroid height:
    QA_total = C_top * A_top + C_bot * A_bot + C_fspar * A_fspar + C_rspar * A_rspar

    for k in range(len(str_x0_lower)):
        QA_total = QA_total + A_str * dchord_lower[k]

    for l in range(len(str_x0_upper)):
        QA_total = QA_total + A_str * dchord_upper[l]

    A_total = A_top + A_bot + A_fspar + A_rspar + (len(str_x0_lower) + len(str_x0_upper)) * A_str
    centroid_z = QA_total / A_total

    # calculate moment of inertia around section centroids: (skin panels only)
    moi_fspar = 1 / 12 * t_fspar * l_fspar ** 3
    moi_rspar = 1 / 12 * t_rspar * l_rspar ** 3
    moi_top = 1 / 12 * t_top * l_top ** 3 * np.sin(np.deg2rad(3.36)) ** 2
    moi_bot = 1 / 12 * t_bot * l_bot ** 3 * np.sin(np.deg2rad(3.775)) ** 2

    # calculate I_xx of each section around neutral axis
    I_xx_fspar = moi_fspar + A_fspar * (C_fspar - centroid_z) ** 2
    I_xx_rspar = moi_rspar + A_rspar * (C_rspar - centroid_z) ** 2
    I_xx_top = moi_top + A_top * (C_top - centroid_z) ** 2
    I_xx_bot = moi_bot + A_bot * (C_bot - centroid_z) ** 2

    # calculate I_xx of each section around neutral axis (stringers):
    f = sp.interpolate.interp1d(str_sec_lower, str_lower, kind="previous", fill_value="extrapolate")
    g = sp.interpolate.interp1d(str_sec_upper, str_upper, kind="previous", fill_value="extrapolate")

    I_xx_str_lower = 0
    I_xx_str_upper = 0
    lowerstr = str(int(f(y)))
    upperstr = str(int(g(y)))
    active_lower = [int(d)-1 for d in lowerstr if d.isdigit()]
    active_upper = [int(e)-1 for e in upperstr if e.isdigit()]

    for m in range(len(active_lower)):
        I_xx_str_lower = I_xx_str_lower + A_str * (dchord_lower[active_lower[m]] - centroid_z) ** 2
    for n in range(len(active_upper)):
        I_xx_str_upper = I_xx_str_upper + A_str * (dchord_upper[active_upper[n]] - centroid_z) ** 2

    # sum inertias
    Ixx_y = I_xx_fspar + I_xx_rspar + I_xx_top + I_xx_bot + I_xx_str_lower + I_xx_str_upper

    return Ixx_y


def bendingDisplacement(cr, ct, b, t_fspar, t_rspar, t_top, t_bot, A_str, str_x0_upper, str_x0_lower, phi1, phi2, str_theta_upper, str_theta_lower, str_sec_upper, str_sec_lower, E):
    def M(y):
        return moment(y)

    def I(y):
        return wingboxmoi(y, cr, ct, b, t_fspar, t_rspar, t_top, t_bot, A_str, str_x0_upper, str_x0_lower, phi1, phi2, str_theta_upper, str_theta_lower, str_sec_upper, str_sec_lower)

    def f_main(y):
        return M(y) / (E * I(y))

    firstDer = [0]
    v = [0]

    stepsize = 1 / 1000
    idx = 0
    for i in np.arange(0, b/2, stepsize):
        dx = stepsize
        dA1 = f_main(i) * dx + 0.5 * (f_main(i + dx) - f_main(i)) * dx
        firstDer.append(firstDer[idx] + dA1)

        dA2 = firstDer[idx] * dx + 0.5 * (firstDer[idx+1] - firstDer[idx]) * dx
        v.append(v[idx] + dA2)

        idx = idx + 1

    perc = round(v[-1] / b * 100, 3)
    limitperc = perc / 15 * 100

    print("Tip bending deflection: " + str(round(v[-1], 2)) + "m (" + str(round(limitperc, 1)) + "% of limit)")

    plt.title("Bending deflection")
    plt.xlabel("Distance from root [m]")
    plt.ylabel("Deflection [m]")
    plt.plot(np.linspace(0, b/2, len(v)), v)
    os.chdir('graphs')
    plt.savefig(f"deflection_diagram_n_{n}.pdf", format="pdf")
    os.chdir('..')
    plt.show()


def TorsionalConstant(t_fspar, t_rspar, t_top, t_bot, y, cr, ct, b):
    c = cr + (ct - cr) / (b / 2) * y

    l_fspar = c * 0.531 / 3.069
    l_rspar = c * 0.327 / 3.069
    l_top = c * 1.636 / 3.069
    l_bot = c * 1.637 / 3.069

    dst = l_fspar / t_fspar + l_rspar / t_rspar + l_top / t_top + l_bot / t_bot

    A1 = l_top * np.cos(np.radians(3.775)) * l_rspar
    A2 = 0.5 * l_top * np.cos(np.radians(3.775)) * l_top * np.sin(np.radians(3.775))
    A3 = 0.5 * l_bot * np.cos(np.radians(3.36)) * l_bot * np.sin(np.radians(3.36))
    A = A1 + A2 + A3

    J = (4 * A * A) / dst

    return J


def TwistDisplacement(t_fspar, t_rspar, t_top, t_bot, cr, ct, b, G):
    torque_func = get_torque_diagram(a0, a10, n=2.866, show_plots=False)

    def J(y):
        return TorsionalConstant(t_fspar, t_rspar, t_top, t_bot, y, cr, ct, b)

    def f_twist(y):
        return torque_func(y) / (G * J(y))

    dtheta = []
    theta = [0]

    stepsize = 1 / 1000
    idx = 0
    for i in np.arange(0, 8.5 , stepsize):
        dx = stepsize
        dA1 = f_twist(i) * dx + 0.5 * (f_twist(i + dx) - f_twist(i)) * dx
        theta.append(theta[idx] + dA1)
        dtheta.append(f_twist(i))
        idx = idx + 1

    perclimit = round(np.degrees(theta[-1]) / 10 * 100, 1)
    # displ,error = sp.integrate.quad(lambda y: f_twist(y), 0, b / 2)
    print("Tip torsional deflection: " + str(round(np.degrees(theta[-1]), 2)) + " degrees (" + str(perclimit) + "% of limit)")

    plt.title("Twist along the wing")
    plt.xlabel("Distance from the root [m]")
    plt.ylabel("Twist [deg]")
    plt.plot(np.linspace(0, b / 2, len(theta)), np.degrees(theta))
    os.chdir('graphs')
    plt.savefig(f"twist_diagram_n_{n}.pdf", format="pdf")
    os.chdir('..')
    plt.show()

# croot = 3.07
# ctip = 1.22
# span = 17.85
# t_fspar = 0.001
# t_rspar = 0.001
# t_top = 0.001
# t_bot = 0.001
# str_area = 0.000040
# emodulus = 70 * 10 ** 9
# shearmodulus = 27 * 10 ** 9
#
# bendingDisplacement(croot, ctip, span, t_fspar, t_rspar, t_top, t_bot, str_area, str_x0_upper, str_x0_lower, np.deg2rad(3.775), np.deg2rad(3.36), str_theta_upper,str_theta_lower, str_sec_upper, str_sec_lower, emodulus)
TwistDisplacement(t_fspar, t_rspar, t_top, t_bot, croot, ctip, span, shearmodulus)

# bendingDisplacement(croot, ctip, span, t_fspar, t_rspar, t_top, t_bot, str_area, str_x0_upper, str_x0_lower, np.deg2rad(phi1), np.deg2rad(phi2), str_theta_upper,str_theta_lower, str_sec_upper, str_sec_lower, emodulus)
# TwistDisplacement(t_fspar, t_rspar, t_top, t_bot, croot, ctip, span, shearmodulus)