import numpy as np
from math import pi, sin, cos
import os
import matplotlib.pyplot as plt
from Forces import get_shear_and_moment
from scipy import interpolate
from main import a0, a10

config = [{"validity": [0, 0.05],
           "setup": {
               "plate_thickness [mm]": 3,
               "stringers_top": 18,
               "stringers_bottom": 14}
           }, {
              "validity": [0.05, 0.1],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 18,
                  "stringers_bottom": 14}
            }, {
              "validity": [0.1, 0.15],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 18,
                  "stringers_bottom": 14}

          }, {
              "validity": [0.15, 0.2],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 18,
                  "stringers_bottom": 12}
          }, {
              "validity": [0.2, 0.25],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 18,
                  "stringers_bottom": 12}
          }, {
              "validity": [0.25, 0.3],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 18,
                  "stringers_bottom": 10}
          }, {
              "validity": [0.3, 0.35],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 16,
                  "stringers_bottom": 10}

          }, {
              "validity": [0.35, 0.4],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 16,
                  "stringers_bottom": 10}
          }, {
              "validity": [0.4, 0.45],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 14,
                  "stringers_bottom": 8}
          }, {
              "validity": [0.45, 0.5],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 12,
                  "stringers_bottom": 8}
          }, {
              "validity": [0.5, 0.55],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 12,
                  "stringers_bottom": 8}
          }, {
              "validity": [0.55, 0.6],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 10,
                  "stringers_bottom": 6}
          }, {
              "validity": [0.6, 0.65],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 10,
                  "stringers_bottom": 6}
          }, {
              "validity": [0.65, 0.7],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 10,
                  "stringers_bottom": 4}
          }, {
              "validity": [0.7, 0.75],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 8,
                  "stringers_bottom": 4}
          }, {
              "validity": [0.75, 0.8],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 8,
                  "stringers_bottom": 3}
          }, {
              "validity": [0.8, 0.85],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 8,
                  "stringers_bottom": 3}
          }, {
              "validity": [0.85, 0.9],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 8,
                  "stringers_bottom": 3}
          }, {
              "validity": [0.9, 0.95],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 8,
                  "stringers_bottom": 3}
          }, {
              "validity": [0.95, 1.0],
              "setup": {
                  "plate_thickness [mm]": 3,
                  "stringers_top": 8,
                  "stringers_bottom": 3}
          }
          ]

test_config = [{
    'validity': [0, 1.0],
    'setup': {
        "plate_thickness [mm]": 2.5,
        "stringers_top": 0,
        "stringers_bottom": 0
    }
}]


def sum_dict(a_dict, b_dict):
    for key in b_dict:
        if key in a_dict.keys():
            if key not in ['a_t', 'a_b']:
                a_dict[key] = np.concatenate((a_dict[key], b_dict[key]))
        else:
            a_dict[key] = b_dict[key]
    return a_dict


def get_variable_at_distance_from_root(y: "Distance from root [m]"):
    # SEE PICTURE FOR WHAT THESE DISTANCES MEAN
    # All output variables are either in radians, mm or dimensionless
    length = 1634.5

    # Top side lengths and angle
    a_t = 3.365 * pi / 180
    t_1 = 289.5
    t_2 = length / cos(a_t)
    t_3 = 193.4
    t_4 = (t_1 + t_3) / 2

    # Bottom side lengths and angle
    a_b = 3.775 * pi / 180
    b_1 = 241.5
    b_2 = length / cos(a_b)
    b_3 = 133.6
    b_4 = (b_1 + b_3) / 2

    variable_names = dir()
    taper_ratio = 0.398
    wingspan = 17.85

    factor = y * (taper_ratio - 1) / (wingspan / 2) + 1
    variables = dict()
    for name in variable_names:
        if name not in ["a_t", "a_b"]:  # The angles don't change
            variables[name] = eval(name) * factor
        else:
            variables[name] = eval(name)

    return variables


class AnalyzeCrossSection:
    def __init__(self, specific_config):
        self.config = specific_config

        # We assume an I-beam stringer
        self.stringer_height = 70  # [mm]
        self.stringer_width = 30  # [mm]
        self.stringer_thickness = 3  # [mm]
        self.stringer_ixx = 0  # to be set by calculate_stringer_properties function
        self.stringer_area = 0  # to be set by calculate_stringer_properties function

        # Basic information about the wing
        self.taper_ratio = 0.398  # [-]
        self.wingspan = 17.85  # [m]

    def calculate_stringer_properties(self):
        i_xx_stringer = 2 * (1 / 12 * self.stringer_width / 2 * self.stringer_thickness ** 3 +
                             self.stringer_thickness * self.stringer_width * (self.stringer_height / 2) ** 2)
        i_xx_stringer += 1 / 12 * self.stringer_thickness * self.stringer_height ** 3
        self.stringer_ixx = i_xx_stringer
        self.stringer_area = (2 * self.stringer_width + self.stringer_height
                              - 2 * self.stringer_thickness) * self.stringer_thickness

    def moi_stringer(self, d: int):
        steiner = self.stringer_area * (d - self.stringer_height / 2) ** 2
        return steiner + self.stringer_ixx

    def moi_box(self,
                t: "plate thickness [mm]",
                centroid: "distance above the no-stringer-centroid [mm]",
                data: "dict containing all data from 'get_variable_at_distance_from_root'"):

        # I will assume that the wing box is made up of 6 plates, 3 above centroid and 3 below.
        moi_t_1 = 1 / 12 * t * data['t_1'] ** 3 + data['t_1'] * t * (data['t_1']/2 - centroid) ** 2
        moi_t_3 = 1 / 12 * t * data['t_3'] ** 3 + data['t_3'] * t * (data['t_3']/2 - centroid) ** 2
        moi_t_2 = data['t_2'] ** 3 * t * sin(data['a_t']) ** 2 / 12 + data['t_2'] * t * (data['t_4'] - centroid) ** 2

        moi_b_1 = 1 / 12 * t * data['b_3'] ** 3 + data['b_3'] * t * (data['b_1']/2 + centroid) ** 2
        moi_b_3 = 1 / 12 * t * data['b_3'] ** 3 + data['b_3'] * t * (data['b_3']/2 + centroid) ** 2
        moi_b_2 = data['b_2'] ** 3 * t * sin(data['a_b']) ** 2 / 12 + data['b_2'] * t * (data['b_4'] + centroid) ** 2

        moi_plate_total = moi_t_1 + moi_t_2 + moi_t_3 + moi_b_1 + moi_b_2 + moi_b_3
        return moi_plate_total

    def find_moi(self,
                 y: "Distance from root [m]",
                 section_config: "Section specific config"):

        data = get_variable_at_distance_from_root(y=y)
        spacing_top = data['length'] / (section_config['setup']['stringers_top'] - 1)
        spacing_bottom = data['length'] / (section_config['setup']['stringers_bottom'] - 1)
        # centroid = self.find_centroid(section_config=section_config, data=data)
        centroid = 0

        def d_top(x: "Distance from the front of the wingbox [mm]"):
            return x * (data['t_3'] - data['t_1']) / data['length'] + data['t_1']

        def d_bottom(x: "Distance from the front of the wingbox [mm]"):
            return x * (data['b_3'] - data['b_1']) / data['length'] + data['b_1']

        # We can now place our stringers:
        i_xx_stringers = 0
        i_xx_plate = self.moi_box(t=section_config['setup']["plate_thickness [mm]"], centroid=centroid, data=data)

        for x in range(section_config['setup']['stringers_top']):
            x_val = x * spacing_top  # Distance from the left of wingbox of a specific stringer
            i_xx_stringers += self.moi_stringer(d_top(x_val) + centroid)

        for x in range(section_config['setup']['stringers_bottom']):
            x_val = x * spacing_bottom  # Distance from the left of wingbox of a specific stringer
            i_xx_stringers += self.moi_stringer(d_bottom(x_val) - centroid)

        i_xx_total = i_xx_stringers + i_xx_plate

        return i_xx_total, data

    def find_centroid(self,
                      section_config: "Section specific config",
                      data: "dict containing all data from 'get_variable_at_distance_from_root'"):
        # We don't care about the plates of the wing box, our chosen coordinate system location is at the centroid of
        # the wing box for the case that there are no stringers, but our wing box centroid will change at each location
        # Depending on the number and location of stringers
        area_plate = (data['t_1'] + data['t_2'] + data['t_3'] + data['b_1'] + data['b_2'] + data['b_3']) * \
                     section_config['setup']['plate_thickness [mm]']
        stringers_top = section_config['setup']['stringers_top']
        stringers_bottom = section_config['setup']['stringers_bottom']
        spacing_top = data['length'] / (stringers_top - 1)
        spacing_bottom = data['length'] / (stringers_bottom - 1)

        def d_top(x: "Distance from the front of the wingbox [mm]"):
            return x * (data['t_3'] - data['t_1']) / data['length'] + data['t_1']

        def d_bottom(x: "Distance from the front of the wingbox [mm]"):
            return x * (data['b_3'] - data['b_1']) / data['length'] + data['b_1']

        i_a_top = sum([d_top(x * spacing_top) for x in range(stringers_top)]) * self.stringer_area
        i_a_bottom = sum([d_bottom(x * spacing_bottom) for x in range(stringers_bottom)]) * self.stringer_area

        centroid = (i_a_top - i_a_bottom) / (area_plate + (stringers_bottom + stringers_top) * self.stringer_area)
        return centroid

    def get_i_xx(self, plot=False, return_data=False, steps=10 ** 3):
        self.calculate_stringer_properties()
        i_xx_list, y_list = np.array([]), np.array([])
        data_list = {}
        for section in self.config:
            y_min, y_max = section['validity']  # These are the percentages of half-wingspan
            assert y_max > y_min
            y_min, y_max = y_min * 0.5 * self.wingspan, y_max * 0.5 * self.wingspan
            y_section = np.linspace(y_min, y_max, int((y_max - y_min) * steps), dtype="float64")[:-1]
            i_xx, data = self.find_moi(y_section, section)
            data_list = sum_dict(data_list, data)
            i_xx_list = np.concatenate((i_xx_list, i_xx))
            y_list = np.concatenate((y_list, y_section))

        if plot:
            plt.title("Moment of inertia along span")
            plt.plot(y_list, i_xx_list)
            plt.xlabel("Distance from root [m]")
            plt.ylabel("Moment of Inertia [mm^4]")
            plt.show()
        if return_data:
            return y_list, i_xx_list, data_list
        return y_list, i_xx_list

    def get_stress(self, plot=False, give_function=False, n=2.866, state="compression"):  # The normal stress
        y, i_xx, data = self.get_i_xx(plot=False, return_data=True)
        _, moment = get_shear_and_moment(a0, a10, show_plots=False, n=n, fuels=[0])
        if n > 0 and state == "compression":
            if plot:
                plt.title("Compressive normal stress in top plate")
            y_max = -data['t_1'] * (10 ** -3)
        elif n <= 0 and state == "compression":
            if plot:
                plt.title("Compressive normal stress in bottom plate")
            y_max = data['b_1'] * (10 ** -3)
        else:
            raise Exception("The program should only be used for compressive stress")

        normal_approx = moment(y) * y_max / (i_xx * 10 ** -12)  # We return the answer in Pa

        if plot:
            plt.plot(y, normal_approx)
            plt.xlabel("Distance from root [m]")
            plt.ylabel("Normal stress [Pa]")
            os.chdir('graphs_wp5')
            plt.savefig('normal_stress.pdf', format='pdf')
            plt.show()
            os.chdir('..')

        if give_function:
            f = interpolate.interp1d(y, normal_approx, kind="cubic", fill_value='extrapolate')
            return f

        return y, normal_approx

    def get_i_xx_per_section(self, p_location: "moment of inertia at percentage of section, per section", n=2.866):
        stress = self.get_stress(give_function=True, n=n)
        self.calculate_stringer_properties()
        values = []
        for section in self.config:
            y_min, y_max = section['validity']
            y = y_min + p_location * (y_max - y_min)
            # of inertia will be the lowest
            i_xx, data = self.find_moi(y, section)
            values.append({'i_xx': i_xx,
                           'y': y,
                           'data': data,
                           'n_stress': stress(y),
                           'l_section': y_max - y_min})

        return values, self.config, self.stringer_area


cross_section = AnalyzeCrossSection(config)
#cross_section.get_i_xx(plot=False)
#cross_section.get_stress(plot=True, give_function=False, n=2.866, state="compression") #TOP PLATE
cross_section.get_stress(plot=True, give_function=False, n=-1, state="compression") #BOTTOM PLATE

#for i in np.arange(0,8.925,0.44625):  #Gives you the chord length at the spanwise position of each rib
 #   j = (get_variable_at_distance_from_root(y=i)['length'])/1000  # at 2 m from root
  #  print(j)

