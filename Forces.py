import os


v = 200.62877691896543
rho = 0.28736810870783724
q = 0.5 * rho * v ** 2


def get_shear_and_moment(dataset_0, dataset_10, show_plots=False, n=1, fuels=None):
    global new_y
    if fuels is None:
        fuels = [0, 0.3, 0.86]
    from numpy import array, sin, cos, radians, linspace
    from scipy import interpolate
    from math import floor
    import matplotlib.pyplot as plt
    from wingWeightDistribution import wingStructureWeight, wingFuelDistribution

    _, wing_weight_function = wingStructureWeight()
    middle = int(floor(len(dataset_0['y'])/2))
    y = array(dataset_0["y"])
    c = array(dataset_0["c"])
    Cl_10 = array(dataset_10["Cl"])
    Cl_0 = array(dataset_0["Cl"])
    Cl = Cl_0 + (0.3583 - Cl_0[middle]) / (Cl_10[middle] - Cl_0[middle]) * (Cl_10 - Cl_0)
    Cd_0 = array(dataset_0["ICd"])
    Cd_10 = array(dataset_10["ICd"])
    Cd = Cd_0 + (0.002 - Cd_0[middle]) / (Cd_10[middle] - Cd_0[middle]) * (Cd_10 - Cd_0)
    v = 200.62877691896543
    rho = 0.28736810870783724
    q = 0.5 * rho * v**2

    graphs = []
    for fuel in sorted(fuels):

        structural_weight = wing_weight_function(y)
        fuel_y, fuel_x = wingFuelDistribution(fuel)
        fuel_weight = interpolate.interp1d(fuel_y, fuel_x, kind="cubic", fill_value='extrapolate')

        l_aero = list(c * q * Cl * n)
        f_aero = interpolate.interp1d(y, l_aero, kind="cubic", fill_value='extrapolate')

        # Local lift
        l = list((Cl*cos(radians(1.73)) - sin(radians(1.73))*Cd) * n*c*q
                 - array(structural_weight) - array(fuel_weight(y)))
        f_l = interpolate.interp1d(y, l, kind="cubic", fill_value='extrapolate')

        if n == 1:
            y_normal = linspace(0, 8.9, 10**3)
            plt.plot(y_normal, f_l(y_normal))
            plt.title(f"Resultant normal distribution @{fuel*100}% fuel")
            plt.ylabel("Local Normal force [N]")
            plt.xlabel("Distance from root [m]")
            os.chdir("graphs")
            plt.savefig(f"normal_distribution_n_1_f_{fuel}.pdf", format="pdf")
            os.chdir("..")
            plt.show()

        # We can now find the shear force diagram
        # You can try to understand this, but I believe it works
        def integrate_from_x_to_l(func, wingspan, step=100, multiplier=1):
            step_size = 1/step
            y_values = list(range(0, floor(wingspan * step)))[::-1]
            res = [func(y_values[-1]) * step_size]
            for i in y_values:
                val = i / step + step_size/2
                res.insert(0, res[0] + func(val)*step_size*multiplier)
            y_values = y_values[::-1]
            y_values = [i/step for i in y_values]
            return array(y_values), res[1:]

        new_y, shear_points = integrate_from_x_to_l(f_l, max(y), step=1000)
        shear = interpolate.interp1d(new_y, shear_points, kind="cubic", fill_value='extrapolate')
        new_y, moment_points = integrate_from_x_to_l(shear, max(y), step=1000)
        moment = interpolate.interp1d(new_y, moment_points, kind="cubic", fill_value='extrapolate')
        graphs.append({'shear': shear,
                       'moment': moment,
                       'lift': f_aero,
                       'fuel': fuel_weight,
                       'fuel_p': fuel,
                       'weight': wing_weight_function})

    if show_plots:
        os.chdir('graphs')

        for graph in graphs:
            plt.title(f"Shear force diagram n={n}")
            plt.plot(new_y, graph["shear"](new_y), label=f"{graph['fuel_p']*100}% fuel")
        plt.xlabel("Distance from root [m]")
        plt.ylabel("Shear force [N]")
        plt.legend()
        plt.savefig(f"shear_force_n_{n}.pdf", format='pdf')
        plt.show()

        plt.title(f"Moment diagram n={n}")
        for graph in graphs:
            plt.plot(new_y, graph['moment'](new_y), label=f"{graph['fuel_p']*100}% fuel")
        plt.xlabel("Distance from root [m]")
        plt.ylabel("Moment Force [Nm]")
        plt.legend()
        plt.savefig(f"moment_force_n_{n}.pdf", format='pdf')
        plt.show()

        # plt.title(f"Loading diagram n={n}")
        # plt.xlabel("Distance from root [m]")
        # plt.ylabel("Load [N/m]")
        # plt.plot(new_y, graphs[1]['lift'](new_y), label="Lift")
        # plt.plot(new_y, (-1) * graphs[1]['weight'](new_y), label=f"Structural")
        # plt.plot(new_y, (-1)*graphs[1]['fuel'](new_y), label=f"{graphs[1]['fuel_p']*100}% Fuel")
        # plt.grid()
        # plt.legend()
        # plt.savefig(f"loading_diagram_n_{n}.pdf", format="pdf")
        os.chdir('..')
        plt.show()

    return graphs[0]['shear'], graphs[0]['moment']


def get_lift(dataset):
    from scipy import interpolate, integrate
    from numpy import array

    y = dataset["y"]
    Cl = dataset["Cl"]
    c = dataset["c"]
    l = list(array(c) * q * array(Cl))  # Local drag
    f_l = interpolate.interp1d(y, l, kind="cubic", fill_value='extrapolate')  # Lift force interpolation

    lift = integrate.quad(f_l, min(y), max(y))[0]

    return lift


def get_drag(dataset):
    from scipy import interpolate, integrate
    from numpy import array

    y = dataset["y"]
    Cd = dataset["ICd"]
    c = dataset["c"]
    d = list(array(c) * q * array(Cd))  # Local drag
    f_d = interpolate.interp1d(y, d, kind="cubic", fill_value='extrapolate')  # Drag force interpolation

    drag = integrate.quad(f_d, min(y), max(y))[0]

    return drag


def get_torque_diagram(dataset_0, dataset_10, n=1, show_plots=True):
    from scipy import interpolate
    import matplotlib.pyplot as plt
    from numpy import array
    from math import floor

    def integrate_from_x_to_l(func, wingspan, step=100, multiplier=1):
        step_size = 1/step
        y_values = list(range(0, floor(wingspan * step)))[::-1]
        res = [func(y_values[-1]) * step_size]
        for i in y_values:
            val = i / step + step_size/2
            res.insert(0, res[0] + func(val)*step_size*multiplier)
        y_values = y_values[::-1]
        y_values = [i/step for i in y_values]
        return y_values, res[1:]

    y = dataset_0["y"]
    middle = int(len(y)/2)
    Cmc4_0 = array(dataset_0["Cmc4"])
    Cmc4_10 = array(dataset_10["Cmc4"])
    Cmc4 = Cmc4_0 + (0.08 - Cmc4_0[middle]) / (Cmc4_10[middle] - Cmc4_0[middle]) * (Cmc4_10 - Cmc4_0)
    Cl_0 = array(dataset_0["Cl"])
    Cl_10 = array(dataset_10["Cl"])
    Cl = Cl_0 + (0.3583 - Cl_0[middle]) / (Cl_10[middle] - Cl_0[middle]) * (Cl_10 - Cl_0)
    c = array(dataset_0["c"])
    v = 200.62877691896543
    rho = 0.28736810870783724
    q = 0.5 * rho * v**2

    cm_torque = array(Cmc4[middle:]) + n * array(Cl[middle:]) * 0.61
    torque = cm_torque * q * array(c[middle:])
    torque_func = interpolate.interp1d(y[middle:], torque, kind="cubic", fill_value='extrapolate')
    y_val, torque = integrate_from_x_to_l(torque_func, y[-1], step=1000)

    if show_plots:
        plt.title(f"Torque for n={n}")
        plt.ylabel("Torque [Nm]")
        plt.xlabel("Distance from root [m]")
        plt.plot(y_val, torque)
        os.chdir('graphs')
        plt.savefig(f'torque_force_n_{n}.pdf', format="pdf")
        os.chdir('..')
        plt.show()

    return torque_func
