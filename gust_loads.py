from math import *
import matplotlib.pyplot as plt
import numpy as np
from isa import ISA


def make_gust_load(altitude=0, condition="lift_off"):
    if condition == "lift_off":
        W = 9416.307 * (0.990*0.995*0.995) * 9.81
        cl_max = 2.2
    elif condition == "cruise":
        W = (9416.307 * (0.99*0.98*0.95*0.95)) * 9.81
        cl_max = 1.83
    elif condition == "climb":
        W = (9416.307 * (0.99*0.98*0.95*0.99)) * 9.81  # I take 0.99 as half of the acceleration to cruise
        cl_max = 1.83
    elif condition == "descent":
        W = (9416.307 * (0.99*0.98*0.95*0.95*0.5949*0.99)) * 9.81
        cl_max = 2.4
    else:
        raise Exception("Please pass a correct flight condition")

    isa = ISA()
    rho = isa.get(altitude)[1]  # Density
    t = isa.get(altitude)[0]
    g = 9.81  # acceleration
    c = 2.57  # Mean Geometric chord
    Vc = 200.68  # 0.68 * sqrt(1.4 * 287 * t)
    Vd = Vc / 0.8  # Dive speed [m/s]
    S = 38.3  # Surface Area [m^2]
    Clalph = 6.26  # Cl alpha wing
    rho0 = 1.225  # Sea level density
    MLW = 6878 * g  # N
    MTOW = 9678 * g  # N
    ZFW = 6178 * g  # N
    Zmo = 12496.8  # max operating ceiling in m
    Uds2 = 0  # Design gust velocities and load factor increasments
    Uds3 = 0
    Uds4 = 0
    dn2 = 0
    dn3 = 0
    dn4 = 0
    U2 = 0
    U3 = 0
    U4 = 0
    load = []
    time = []
    loadfactor = []


    Vs1 = sqrt(2*W / (S*cl_max*rho))  # Stall speeds without active flaps
    if altitude <= 4572:  # Determination of Uref
        Uref = 17.07 - ((17.07 - 13.41) / 4572) * altitude
    elif 4572 < altitude <= 18288:
        Uref = 13.41 - (13.41 - 6.36) / (18288 - 4572) * (18288 - altitude)

    mu = (2 * W / S) / (rho * c * Clalph * g)
    Kg = (0.88 * mu) / (5.3 + mu)
    Vb = Vs1 * sqrt(1 + (Kg * rho0 * Uref * Vc * Clalph) / (2 * W / S))  # Max gust intensity speed
    Velocity = Vb
    Velocity2 = Vc
    Velocity3 = Vc / 0.8

    R1 = MLW / MTOW  # Needed constants for calculating design speed
    R2 = ZFW / MTOW
    Fgz = 1 - Zmo / 76200
    Fgm = sqrt(R2 * tan((pi * R1) / 4))
    Fg = 0.5 * (Fgz + Fgm)
    lam = (W / S) / Clalph * 2 / rho / Velocity / g

    for H in range(9, 108):  # Gust loads and lines to Vb
        Uds = Uref * Fg * (H / 107) ** (1 / 6)
        if Uds > Uds2:
            Uds2 = Uds

        omega = pi * Velocity / H
        for t in np.arange(0, 2 * pi / omega, pi / omega / 1000):
            dn = (Uds / (2 * g)) * (omega * sin(omega * t) + (1 / (1 + (omega * lam) ** (-2))) * (
                        ((1 / lam) * exp(-t / lam)) - ((1 / lam) * cos(omega * t)) - (omega * sin(omega * t))))
            if dn > dn2:
                dn2 = dn
        loadfactor.append(dn2)
        # dn2 = 0

        for s in np.arange(0, (2 * H), 2 * H / 1000):
            U = Uds / 2 * (1 - cos((pi * s) / H))
            if U > U2:
                U2 = U
        load.append(U2)
        time.append(2 * H / 0.3048)


    for H in range(9, 108):  # Gust loads and lines for Vb to Vc
        Uds = Uref * Fg * (H / 107) ** (1 / 6)
        if Uds > Uds3:
            Uds3 = Uds

        omega = pi * Velocity2 / H
        for t in np.arange(0, 2 * pi / omega, pi / omega / 1000):
            dn = (Uds / (2 * g)) * (omega * sin(omega * t) + (1 / (1 + (omega * lam) ** (-2))) * (
                        ((1 / lam) * exp(-t / lam)) - ((1 / lam) * cos(omega * t)) - (omega * sin(omega * t))))
            if dn > dn3:
                dn3 = dn

        for s in np.arange(0, (2 * H), 2 * H / 1000):
            U = Uds / 2 * (1 - cos((pi * s) / H))
            if U > U3:
                U3 = U

    Uref = 0.5 * Uref  # This is needed to compute the dive speed d

    for H in range(9, 108):  # Gust loads and lines for Vc to Vd

        Uds = Uref * Fg * (H / 107) ** (1 / 6)
        if Uds > Uds4:
            Uds4 = Uds

        omega = pi * Velocity3 / H
        for t in np.arange(0, 2 * pi / omega, pi / omega / 1000):
            dn = (Uds / (2 * g)) * (omega * sin(omega * t) + (1 / (1 + (omega * lam) ** (-2))) * (
                        ((1 / lam) * exp(-t / lam)) - ((1 / lam) * cos(omega * t)) - (
                            omega * sin(omega * t))))  # load factor increasement due to gusts
            if dn > dn4:
                dn4 = dn

        for s in np.arange(0, (2 * H), 2 * H / 1000):
            U = Uds / 2 * (1 - cos((pi * s) / H))
            if U > U4:
                U4 = U

    plt.plot([0, Vb], [1, 1 + dn2], color="black")
    plt.plot([0, Vb], [1, 1 - dn2], color="black")
    plt.plot([Vb, Vc], [1 + dn2, 1 + dn3], color="black")
    plt.plot([Vb, Vc], [1 - dn2, 1 - dn3], color="black")
    plt.plot([Vc, Vd], [1 + dn3, 1 + dn4], color="black")
    plt.plot([Vc, Vd], [1 - dn3, 1 - dn4], color="black")
    plt.plot([Vd, Vd], [1 + dn4, 1 - dn4], color="black")
    # I remove the plt.show here so these plots get added to the vn graphs
