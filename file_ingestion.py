from typing import Dict, List


def ingest(filename: str) -> Dict[str, List]:
    data = open(filename, 'r').readlines()[21:101]

    def get_list(text, start, end):
        lst = [float(line[start:end].replace(" ", "").replace("\n", "")) for line in text]
        return lst

    y_span = get_list(data, 3, 14)
    chord = get_list(data, 14, 24)
    Ai = get_list(data, 24, 34)
    Cl = get_list(data, 34, 46)
    ICd = get_list(data, 58, 70)
    Cmc4 = get_list(data, 82, 95)

    cleaned_data = {"y": y_span, "c": chord, "Ai": Ai, "Cl": Cl, "ICd": ICd, "Cmc4": Cmc4}
    return cleaned_data

