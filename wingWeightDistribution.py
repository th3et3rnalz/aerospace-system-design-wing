import scipy as sp
from scipy import interpolate, integrate
import matplotlib.pyplot as plt
import numpy as np
import os
from file_ingestion import ingest


def wingStructureWeight():
    m_wing = 624.77309  # [kg] random value
    A_wing = 38.3294190  # [m^2] random value
    g = 9.81

    data = ingest("Datafiles/MainWinga0.txt")  # alpha = 0
    x = data["y"]  # [m]
    y = np.array(data["c"])  # [m]
    yW = y*m_wing*g/A_wing  # [N/m]

    # -b/2<=y<=0 or -8.9216<=y<=0
    x1 = x[0:39]
    y1 = yW[0:39]
    # Function that give a [N/m] value # Absolute value now.
    w1 = sp.interpolate.interp1d(x1, y1, kind='linear', fill_value="extrapolate")

    # 0<y<=b/2 or 0<y<=8.9216
    x2 = x[40:79]
    y2 = yW[40:79]
    # Function that give a [N/m] value # Absolute value now.
    w2 = sp.interpolate.interp1d(x2, y2, kind='linear', fill_value="extrapolate")

    return w1, w2


#  To reach design fuel mass fill the tanks at 86%.
def wingFuelDistribution(percentageFuel):  # percentage written as fraction between 0 and 1, function returns y and q distribution values
    n_comp = 5  # compartments on one side of the wing this means there are at least 4 ribs in one side of the wing
    g= 9.81  # [m/s^2]
    A_cr = 0.701  # [m^2]
    A_t = 0.111  # [m^2]
    di = 5  # [degrees]
    b_half = 8.9216  # [m]
    fuelDensity = 0.795*1000  # [kg/m^3]
    fuselageDiameter = 2  # [m]
    aileronStart = 0.76*b_half  # [m]

    A = [A_t,A_cr,A_t]  # [m^2]
    yA = [-b_half,0,b_half]  # [m]

    A1 = sp.interpolate.interp1d(yA[0:2],A[0:2], kind='linear', fill_value="extrapolate")
    A2 = sp.interpolate.interp1d(yA[1:3],A[1:3], kind='linear', fill_value="extrapolate")
    
    # only right half 0<y<=b/2
    tankSpaceStart = fuselageDiameter/2
    tankSpaceEnd = aileronStart
    tankSpan = (tankSpaceEnd-tankSpaceStart)/n_comp
    tankDetails = [] # [tank_number,tank_span,tank_y0, tank_ycenter, tank_yend, tank_area, tank_volume, percentageStart,percentageEnd,totalPercentage, fillPercentage, weightAccordingToFillStatus,flatLoadDist]
    i=0
    tankStart=tankSpaceStart
    tankCenter = tankSpan/2
    tankEnd = tankStart+tankSpan
    totalVolume = 0
    while i < n_comp:
        if(i>0):
            tankStart += 1*tankSpan
            tankCenter += 1*tankSpan
            tankEnd += 1*tankSpan
        tankArea = A2
        tankVolume = integrate.quad(A2, tankStart, tankEnd)[0]
        # print("Tank ("+str(i)+") Volume: "+str(tankVolume) + " . Tank Start: " + str(tankStart) + " . Tank End: " + str(tankEnd) + " .")
        totalVolume += tankVolume
        tankDetails.append([i,tankSpan,tankStart,tankCenter,tankEnd,tankArea,tankVolume, 0,0,0,0,0,0])
        i+=1
    # print("Total Volume: " + str(totalVolume))
    
    tankDetails.reverse()
    percentageStart = 0
    for tank in tankDetails:
        tank[7] = percentageStart
        percentage = tank[6]/totalVolume
        tank[9] = percentage
        # print("Tank(" + str(tank[0]) + ") Percentage: "+ str(percentage))
        percentageStart += percentage
        tank[8] = percentageStart
    tankDetails.reverse()

    totalWeight = 0
    fuelRemaining = percentageFuel
    tankDetails.reverse()
    for tank in tankDetails:
        if(fuelRemaining <= 0):
            break
        else:
            if(fuelRemaining < tank[9]):
                tank[10] = fuelRemaining/tank[9]
                fuelRemaining = 0
            elif(fuelRemaining >= tank[9]):
                tank[10] = 1
                fuelRemaining -= tank[9]
            tank[11] = tank[6]*tank[10]*fuelDensity*g # weight = tankvolume*tankfill*fueldensity*g
            totalWeight += tank[11]
            tank[12] = tank[11]/tank[1] # distribution = tank_weight/span
    tankDetails.reverse()
    
    flatDistributionY = np.linspace(0, b_half, 1000000)
    flatDistributionQ = []
    
    for y in flatDistributionY:
        for tank in tankDetails:
            if(y<tankSpaceStart):
                flatDistributionQ.append(0)
                break
            elif(y>tankSpaceEnd):
                flatDistributionQ.append(0)
                break
            elif(tank[2]<=y and tank[4]>=y):
                flatDistributionQ.append(tank[12])
                break
    
    for tank in tankDetails:
        doNothing = True
        # print(tank)
        # print("Tank("+str(tank[0])+") Fill Percentage: " + str(round(tank[10]*100,2)) + "%.")
        # print("Tank(" + str(tank[0]) + "); Centre(y, m^2): " + str(round(tank[3],2)) + " Weight [N]: "+ str(round(tank[11],2))+".")
    
    # totals
    fuelMass = totalWeight/g
    fuelVolume = fuelMass/fuelDensity
    # print("Total filled volume[m^3]: " + str(round(fuelVolume*2,2)) + " . Total filled fuel mass [kg]: " + str(round(fuelMass*2,2)) + " .")
    assert len(flatDistributionQ) == len(flatDistributionY)

    return flatDistributionY, flatDistributionQ
    
    
#  per = input("Fuel Percentage: ")
#  y,q = wingFuelDistribution(int(per)/100)

def graph_wing_weight_vs_span():
    _, weight = wingStructureWeight()
    y = np.linspace(0, 8.9, 10**4)
    plt.plot(y, weight(y))
    os.chdir('graphs')
    plt.title("Wing weight vs span")
    plt.xlabel("Distance from root [m]")
    plt.ylabel("Wing weight [N/m]")
    plt.savefig('wing_weight.pdf', format='pdf')
    os.chdir('..')
    plt.show()


def graph_fuel_weight_vs_span(fuels=None):
    if fuels is None:
        fuels = [0.3, 0.86, 1.0]
    assert type(fuels) == list
    assert len(fuels) == 3
    y_0, val_0 = wingFuelDistribution(fuels[0])
    y_1, val_1 = wingFuelDistribution(fuels[1])
    y_2, val_2 = wingFuelDistribution(fuels[2])

    plt.plot(y_0, val_0, label=f"{fuels[0]*100}% fuel")
    plt.plot(y_1, val_1, label=f"{fuels[1]*100}% fuel")
    plt.plot(y_2, val_2, label=f"{fuels[2]*100}% fuel")

    plt.title("Fuel weight vs span")
    plt.xlabel("Distance from root [m]")
    plt.ylabel("Fuel weight [N/m]")
    plt.legend()
    os.chdir('graphs')
    plt.savefig(f'fuel_weight_f0_{fuels[0]}_f1_{fuels[1]}.pdf', format="pdf")
    os.chdir('..')
    plt.show()