import matplotlib.pyplot as plt
from numpy import linspace, sqrt


for zeta in [0.1, 0.3, 0.5, 0.7, 1, 1.5, 10]:
    r = linspace(0, 4, 10**3)
    a = 1 / sqrt((1-r**2)**2 + (2*zeta*r)**2)
    legend = "zeta = "+str(zeta)
    plt.plot(r, a, label=legend)
plt.legend()
plt.title("Amplitude change for ")
plt.xlabel("Normalized amplitude")
plt.ylabel("Frequency ratio")
plt.show()
