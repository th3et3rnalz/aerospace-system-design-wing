from math import sqrt
from isa import ISA
import numpy as np
import matplotlib.pyplot as plt
import os


def make_vn(altitude=0.0, condition="cruise"):
    isa = ISA()
    rho = isa.get(altitude)[1]
    t = isa.get(altitude)[0]
    cl_to_max = 2.2
    cl_land_max = 2.4
    w_mto = 9416.307 * 9.81
    cl_clean_max = 1.83
    s = 38.3
    n_max = 2.1 + (24000 / (10000 + w_mto * 2.20462 / 9.81))
    vc = 200.68  # 0.68* sqrt(1.4 * 287 * t)
    vd = vc / 0.8

    if condition == "lift_off":
        cl_max = cl_to_max
        w = 9416.307 * (0.990*0.995*0.995) * 9.81
        rho = 1.225
    elif condition == "cruise":
        w = (9416.307 * (0.99*0.98*0.95*0.95)) * 9.81
        cl_max = 1.83
    elif condition == "climb":
        w = (9416.307 * (0.99*0.98*0.95*0.99)) * 9.81  # I take 0.99 as half of the acceleration to cruise
        cl_max = cl_to_max
    elif condition == "descent":
        # W = (9416.307 * (0.99 * 0.98 * 0.95 * 0.95 * 0.5949 * 0.995)) * 9.81
        cl_max = 2.4
        w = (9416.307 * (0.99*0.98*0.95*0.95*0.5949*0.995)) * 9.81
    else:
        raise Exception("Please pass a correct flight condition")

    # (0) - zeroth line segment
    v_stall_0 = sqrt(2*w / (s*cl_max*rho))

    def n_0(v):
        n = (v / v_stall_0)**2
        return n

    y_0 = np.linspace(0, v_stall_0 * sqrt(2), 10**3)
    if condition != "cruise":
        y_0 = np.linspace(0, v_stall_0 * sqrt(2), 10 ** 3)
        plt.plot(y_0, n_0(y_0), label="Flaps down")

    # (1) - first line segment
    vs1 = sqrt(2 * w / (s * cl_clean_max * rho))
    va = vs1 * sqrt(n_max)
    vh = vs1

    def n_1(v):
        n = (v / vs1)**2
        return n

    y_1 = np.linspace(0, va, 10**3)
    plt.plot(y_1, n_1(y_1), label=condition)

    # Connection bar between flaps down and cruise
    v_1 = y_0[-1]
    closest_n = 0
    for y in n_1(y_1):
        if y > 2:
            closest_n = y
            break

    v_2 = list(n_1(y_1)).index(closest_n)*va / 10**3
    plt.plot([v_1, v_2], [2, 2], color="black")

    # (2) - second line segment
    y_2 = np.linspace(0, vh, 10**3)
    plt.plot(y_2, (-1)*n_1(y_2), color="black")

    # (3) - third line segment
    plt.plot([va, vd], [n_max, n_max], color="black")

    # (4) - fourth line segment
    x_4 = np.linspace(0, n_max, 10**3)
    plt.plot([vd, vd], [0, n_max], color="black")

    # (5) - fifth line segment
    plt.plot([vc, vd], [-1, 0], color="black")

    # (6) - sixth line segment
    plt.plot([vh, vc], [-1, -1], color="black")

    plt.title(f"vn and gust load diagram @{condition}")
    plt.xlabel("V_eas [m/s]")
    os.chdir('graphs')
    plt.savefig(f"vn_diagram_{condition}.pdf", format="pdf")
    os.chdir('..')
    plt.ylabel("n")
    plt.legend()
    plt.show()


# h = 12496.8
# make_vn(altitude=0, condition="lift_off")
# make_vn(altitude=h/2, condition="climb")
# make_vn(altitude=h, condition="cruise")
# make_vn(altitude=h, condition="descent")
