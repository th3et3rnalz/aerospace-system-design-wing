from math import pi
from moment_of_inertia import cross_section


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def verify_column_buckling(n=2.866, verbose=False):
    i_xx_values, config, stringer_area = cross_section.get_i_xx_per_section(p_location=0.5, n=n)
    E = 71 * 10**6

    section_number = 0
    for section in config:
        data = i_xx_values[section_number]['data']
        stringer_top = section['setup']['stringers_top']
        stringer_bottom = section['setup']['stringers_bottom']
        if section_number == 0:
            # We're in the first section
            K = (1 / 0.7) ** 2
        elif section_number == len(config)-1:
            # We're in the last section
            K = 1 / 4
        else:
            K = 1

        # Let's calculate all the area above the y=0 mark
        area_plate = (data['t_1'] + data['t_2'] + data['t_3'])*section['setup']['plate_thickness [mm]']
        area_total = area_plate + stringer_top*stringer_area

        i_xx_of_section = i_xx_values[section_number]['i_xx']       # mm^4
        y_section = i_xx_values[section_number]['y']
        normal_stress_section = i_xx_values[section_number]['n_stress']
        l_section = i_xx_values[section_number]['l_section'] * 17850/2
        if verbose:
            print('Ixx', i_xx_of_section)
            print('y_section', y_section)
            print('normal_stress_section', normal_stress_section)
            print('l_section', l_section)
            print('area_total', area_total)
            print('K', K)

            print("---------")

        f_crit = (K * pi**2 * E * i_xx_of_section) / l_section**2
        # Critical stress that may occur in the column
        s_crit = f_crit/area_total
        # Now check if the stress in the section exceeds the critical strength

        mos = -s_crit/normal_stress_section
        if mos < 1:
            print(bcolors.FAIL, f"MoS: {mos}", bcolors.ENDC)
            if verbose:
                print("MoS:", mos, ". Critical sigma:", s_crit, "Not a possibility")
        if mos >= 1:
            print(bcolors.OKGREEN, f"MoS: {mos}", bcolors.ENDC)

            if verbose:
                print("MoS:", mos, ". Critical sigma:", s_crit)
        section_number += 1


print("For n=2.866")
verify_column_buckling(n=2.866)
print("For n=-1")
verify_column_buckling(n=-1)